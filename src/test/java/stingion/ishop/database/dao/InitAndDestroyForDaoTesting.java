package stingion.ishop.database.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by ievgen on 3/16/14 7:12 PM.
 */
@ContextConfiguration("/spring-context/database-creation.xml")
public class InitAndDestroyForDaoTesting {

    @Autowired
    ApplicationContext context;

    public void init() {

        System.out.println(InitAndDestroyForDaoTesting.class + " initialization");
        context.getBean("scriptExecStruct");
    }

    public void destroy() {
        context.getBean("scriptExecDrop");
        System.out.println(InitAndDestroyForDaoTesting.class + " destroying");
    }
}
