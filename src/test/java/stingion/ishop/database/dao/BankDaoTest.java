package stingion.ishop.database.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ishop.database.map.Bank;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "/spring-context-test/contexts-bundle-test.xml",
        "/spring-context/database-creation.xml"
})
public class BankDaoTest {

    @Autowired
    private Bank bank;

    @Autowired
    private BankDao bankDao;

    @Autowired
    private InitAndDestroyForDaoTesting initAndDestroyForDaoTesting;

    @Before
    public void init(){
        initAndDestroyForDaoTesting.init();
    }

    @After
    public void destory(){
        initAndDestroyForDaoTesting.destroy();
    }

    @Test
    public void test() {

        String bankName = "stableBank";

        bank.setName(bankName);
        bankDao.save(bank);
        bank = bankDao.getBankById(1);

        Assert.assertEquals(bankName, bank.getName());
        Assert.assertEquals(1L, (long) bank.getId());

        bankDao.delete(bank);
    }
}
