package stingion.ishop.database.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ishop.database.map.CreditCard;
import stingion.ishop.database.util.CardType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "/spring-context-test/contexts-bundle-test.xml",
        "classpath:/spring-context/database-creation.xml"
})
public class CreditCardDaoTest {

    @Autowired
    private CreditCardDao creditCardDao;

    @Autowired
    private InitAndDestroyForDaoTesting initAndDestroyForDaoTesting;

    @Before
    public void init(){
        initAndDestroyForDaoTesting.init();
    }

    @After
    public void destory(){
        initAndDestroyForDaoTesting.destroy();
    }

    @Test
    public void test() {

        long number = 111L;
        CardType cardType = CardType.MasterCard;

        CreditCard creditCard = new CreditCard();

        creditCard.setNumber(number);
        creditCard.setType(cardType);
        creditCardDao.save(creditCard);

        creditCard = creditCardDao.getCreditCardById(1);

        Assert.assertEquals(number, (long) creditCard.getNumber());
        Assert.assertEquals(cardType, creditCard.getType());
    }
}
