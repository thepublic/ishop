package stingion.utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-context-test/database-creation-test.xml")
public class SQLScriptExecutorTest {

    @Autowired
    private String url;

    @Autowired
    @Qualifier("mainScriptsList")
    public void setScriptsMainList(LinkedList<String> scriptsMainList) {
        this.scriptsMainList = scriptsMainList;
    }

    private List<String> scriptsMainList;

    @Autowired
    private String endingScript;

    @Autowired
    private SQLScriptExecutor ess;

    private Connection conn;
    private Statement stmt;

    @Before
    public void beforeClass() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(url);
        stmt = conn.createStatement();
    }

    @Test
    public void testExecute() throws Exception {

        for (String scriptPath : scriptsMainList) {
            ess.execute(scriptPath);
        }

        ResultSet rs = stmt.executeQuery("show databases;");

        boolean IShopDBExist = false;
        while (rs.next()) {
            if (rs.getString(1).equals("ishop_test"))
                IShopDBExist = true;
        }

        boolean expected = true;
        boolean actual = IShopDBExist;

        assertEquals(expected, actual);
    }

    @After
    public void afterClass() throws Exception {
        ess.execute(endingScript);
    }
}
