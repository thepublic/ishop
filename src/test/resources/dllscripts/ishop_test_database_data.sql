use `ishop_test`;

-- ----- USER -----
insert into `user` (`name`) values ('Jimi Hendrix');
insert into `user` (`name`) values ('Ignat Gee');
insert into `user` (`name`) values ('Mister Mix');
insert into `user` (`name`) values ('Andrew Scott');
insert into `user` (`name`) values ('Reptile');

-- ----- BANK -----
insert into `bank` (`name`) values ('Unicredit');
insert into `bank` (`name`) values ('Raiffeisen');
insert into `bank` (`name`) values ('PrivatBank');
insert into `bank` (`name`) values ('UkrSibbank');
insert into `bank` (`name`) values ('Platinum Bank');

-- ----- SELLER -----
insert into `seller` (`name`) values ('Technomir');
insert into `seller` (`name`) values ('Rozetka');
insert into `seller` (`name`) values ('Foxtrot');
insert into `seller` (`name`) values ('Comfy');

-- ----- PRODUCT -----
insert into `product` (`name`, `price`) values ('Laptop Asus x55vd', 480);
insert into `product` (`name`, `price`) values ('Laptop Apple MacBook Air', 1400);
insert into `product` (`name`, `price`) values ('Laptop Dell Inspiron 7720', 1050);
insert into `product` (`name`, `price`) values ('E-book Amazon Kindle 5', 80);
insert into `product` (`name`, `price`) values ('E-book Pocketbook Basic', 105);
insert into `product` (`name`, `price`) values ('Phone Apple iPhone 5 16GB', 750);
insert into `product` (`name`, `price`) values ('Phone Samsung N7100 Galaxy Note 2', 670);

-- ----- PRODUCT_SELLER -----
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Asus x55vd'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Asus x55vd'), 
			(select `id` from seller where `name` = 'Foxtrot')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Asus x55vd'), 
			(select `id` from seller where `name` = 'Comfy')
		   );

insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Apple MacBook Air'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Apple MacBook Air'), 
			(select `id` from seller where `name` = 'Technomir')
		   );

insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Dell Inspiron 7720'), 
			(select `id` from seller where `name` = 'Technomir')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Dell Inspiron 7720'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Laptop Dell Inspiron 7720'), 
			(select `id` from seller where `name` = 'Foxtrot')
		   );

insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'E-book Amazon Kindle 5'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'E-book Amazon Kindle 5'), 
			(select `id` from seller where `name` = 'Foxtrot')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'E-book Amazon Kindle 5'), 
			(select `id` from seller where `name` = 'Comfy')
		   );

insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'E-book Pocketbook Basic'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );

insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Phone Apple iPhone 5 16GB'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Phone Apple iPhone 5 16GB'), 
			(select `id` from seller where `name` = 'Foxtrot')
		   );

insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Phone Samsung N7100 Galaxy Note 2'), 
			(select `id` from seller where `name` = 'Rozetka')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Phone Samsung N7100 Galaxy Note 2'), 
			(select `id` from seller where `name` = 'Foxtrot')
		   );
insert into `product_seller` (`product_id`, `seller_id`) 
	values (
			(select `id` from product where `name` = 'Phone Samsung N7100 Galaxy Note 2'), 
			(select `id` from seller where `name` = 'Comfy')
		   );

-- ----- CREDIT_CARD -----
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			1234571277518812,
			'MasterCard',
			(select `id` from `bank` where `name` = 'PrivatBank'), 
			(select `id` from `user` where `name` = 'Jimi Hendrix')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			6647486058945594,
			'Visa',
			(select `id` from `bank` where `name` = 'Unicredit'), 
			(select `id` from `user` where `name` = 'Jimi Hendrix')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			2237435131126577,
			'Visa',
			(select `id` from `bank` where `name` = 'Raiffeisen'), 
			(select `id` from `user` where `name` = 'Ignat Gee')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			1123729594138004,
			'MasterCard',
			(select `id` from `bank` where `name` = 'UkrSibbank'), 
			(select `id` from `user` where `name` = 'Mister Mix')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			6541296068298653,
			'Visa',
			(select `id` from `bank` where `name` = 'Platinum Bank'), 
			(select `id` from `user` where `name` = 'Andrew Scott')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			5923542550577566,
			'MasterCard',
			(select `id` from `bank` where `name` = 'Platinum Bank'), 
			(select `id` from `user` where `name` = 'Andrew Scott')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			7970188838706486,
			'Visa',
			(select `id` from `bank` where `name` = 'Unicredit'), 
			(select `id` from `user` where `name` = 'Reptile')
		   );
insert into `credit_card` (`number`, `type`, `bank_id`, `user_id`) 
	values(
			2184747417855136,
			'Visa',
			(select `id` from `bank` where `name` = 'PrivatBank'), 
			(select `id` from `user` where `name` = 'Reptile')
		   );