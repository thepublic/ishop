package stingion.ishop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import stingion.ishop.database.CreationDB;
import stingion.ishop.imitation.OrderingQueueFiller;
import stingion.ishop.imitation.OrderingQueueFreer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Starter {
    public static void main(String[] args) throws InterruptedException {

        System.out.println("Entry");

        CreationDB.main(null);

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context/contexts-bundle.xml");

        @SuppressWarnings("unchecked")
        OrderingQueueFiller orderingQueueFiller = (OrderingQueueFiller) context.getBean("orderingQueueFiller");
        OrderingQueueFreer orderingQueueFreer = (OrderingQueueFreer) context.getBean("orderingQueueFreer");

        ExecutorService exec = Executors.newCachedThreadPool();

        exec.execute(orderingQueueFiller);
        exec.execute(orderingQueueFiller);
        exec.execute(orderingQueueFiller);

        exec.execute(orderingQueueFreer);
        exec.shutdown();

        System.out.println("Done");
    }
}
