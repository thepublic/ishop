package stingion.ishop.imitation;

import stingion.ishop.database.map.Ordering;

public interface ObtainingOrdering {
    Ordering obtainNewOrdering();
}
