package stingion.ishop.imitation;

import org.aspectj.lang.JoinPoint;
import stingion.ishop.database.dao.OrderingDao;
import stingion.ishop.database.map.Ordering;

public class AopSubmitAndHandle {
    private OrderingDao orderingDao;

    public OrderingDao getOrderingDao() {
        return orderingDao;
    }

    public void setOrderingDao(OrderingDao orderingDao) {
        this.orderingDao = orderingDao;
    }

    public void onSubmitting(JoinPoint jp) {
        Ordering ordering = (Ordering) jp.getArgs()[0];
        System.out.println("Submitting ...");
        orderingDao.save(ordering);
    }

    public void onHandling(Object retVal) {
        Ordering ordering = (Ordering) retVal;
        System.out.println("Handling ...");
        if (ordering != null)
            orderingDao.update(ordering);
    }

}

