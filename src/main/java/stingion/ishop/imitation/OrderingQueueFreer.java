package stingion.ishop.imitation;

import stingion.ishop.database.map.Ordering;
import stingion.ishop.handle.Handling;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class OrderingQueueFreer implements Runnable {

    private Handling<Ordering> handling;

    private int lowInterval;
    private int highInterval;

    public Handling getHandling() {
        return handling;
    }

    public void setHandling(Handling handling) {
        this.handling = handling;
    }

    public int getLowInterval() {
        return lowInterval;
    }

    public void setLowInterval(int lowInterval) {
        this.lowInterval = lowInterval;
    }

    public int getHighInterval() {
        return highInterval;
    }

    public void setHighInterval(int highInterval) {
        this.highInterval = highInterval;
    }

    public void run() {
        Random rand = new Random();

        Ordering currentOrdering;

        try {
            do {
                currentOrdering = handling.handle();
                TimeUnit.MILLISECONDS.sleep(rand.nextInt(highInterval
                        - lowInterval)
                        + lowInterval);
            } while (currentOrdering != null);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
