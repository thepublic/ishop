package stingion.ishop.imitation;

import stingion.ishop.database.dao.CreditCardDao;
import stingion.ishop.database.dao.ProductDao;
import stingion.ishop.database.map.CreditCard;
import stingion.ishop.database.map.Ordering;
import stingion.ishop.database.map.Product;
import stingion.ishop.database.util.OrderStatus;

import java.util.List;
import java.util.Random;

public class OrderingCreatingRandGenerator implements ObtainingOrdering {
    private ProductDao productDao;
    private CreditCardDao creditCardDao;
    private Random rand = new Random();

    public OrderingCreatingRandGenerator() {
    }

    public OrderingCreatingRandGenerator(ProductDao productDao, CreditCardDao creditCardDao) {
        super();
        this.productDao = productDao;
        this.creditCardDao = creditCardDao;
    }

    public synchronized ProductDao getProductDao() {
        return productDao;
    }

    public synchronized void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    public synchronized CreditCardDao getCreditCardDao() {
        return creditCardDao;
    }

    public synchronized void setCreditCardDao(CreditCardDao creditCardDao) {
        this.creditCardDao = creditCardDao;
    }

    private Ordering create() {
        List<Product> productList = productDao.getProductList();
        List<CreditCard> creditCardList = creditCardDao.getCreditCardList();

        Product product = productList.get(new Random().nextInt(productList
                .size()));
        CreditCard creditCard = creditCardList.get(new Random()
                .nextInt(creditCardList.size()));

        String description = createDescription();
        OrderStatus status = createStatus();

        Ordering ordering = new Ordering();

        ordering.setProduct(product);
        ordering.setCreditCard(creditCard);
        ordering.setDescription(description);
        ordering.setStatus(status);

        return ordering;
    }

    private String createDescription() {
        return "no comments";
    }

    private OrderStatus createStatus() {
        OrderStatus status;
        if (rand.nextInt(10) == 0)
            status = OrderStatus.defective;
        else
            status = OrderStatus.pending;

        return status;
    }

    public synchronized Ordering obtainNewOrdering() {
        return create();
    }
}

