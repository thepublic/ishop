package stingion.ishop.imitation;

import stingion.ishop.database.map.Ordering;
import stingion.ishop.submit.Submitting;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class OrderingQueueFiller implements Runnable {
    private Submitting<Ordering> submitting;
    private ObtainingOrdering obtainOrdering;

    private int orderingAmout;
    private int lowInterval;
    private int highInterval;

    public Submitting<Ordering> getSubmitting() {
        return submitting;
    }

    public void setSubmitting(Submitting<Ordering> submitting) {
        this.submitting = submitting;
    }

    public ObtainingOrdering getObtainOrdering() {
        return obtainOrdering;
    }

    public void setObtainOrdering(ObtainingOrdering obtainOrdering) {
        this.obtainOrdering = obtainOrdering;
    }

    public int getOrderingAmout() {
        return orderingAmout;
    }

    public void setOrderingAmout(int orderingAmout) {
        this.orderingAmout = orderingAmout;
    }

    public int getLowInterval() {
        return lowInterval;
    }

    public void setLowInterval(int lowInterval) {
        this.lowInterval = lowInterval;
    }

    public int getHighInterval() {
        return highInterval;
    }

    public void setHighInterval(int highInterval) {
        this.highInterval = highInterval;
    }

    public void run() {
        Random rand = new Random();
        try {

            for (int i = 0; i < orderingAmout; i++) {
                Ordering ordering = obtainOrdering.obtainNewOrdering();
                System.out.println(ordering);
                submitting.submit(ordering);
                TimeUnit.MILLISECONDS.sleep(rand.nextInt(highInterval
                        - lowInterval)
                        + lowInterval);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

