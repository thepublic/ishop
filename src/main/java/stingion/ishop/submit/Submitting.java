package stingion.ishop.submit;

public interface Submitting<T> {
	void submit(T element);
}
