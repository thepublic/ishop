package stingion.ishop.submit;

import java.util.concurrent.BlockingQueue;

public interface BQSubmitting<T> extends Submitting<T> {
	BlockingQueue<T> getQueue();

	void setQueue(BlockingQueue<T> bq);
}
