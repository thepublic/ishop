package stingion.ishop.submit;

import java.util.concurrent.BlockingQueue;

public class BQSubmittingImpl<T> implements BQSubmitting<T> {
	private BlockingQueue<T> queue;

	public BlockingQueue<T> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<T> queue) {
		this.queue = queue;
	}

	public void submit(T element) {
		try {
			queue.put(element);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

}
