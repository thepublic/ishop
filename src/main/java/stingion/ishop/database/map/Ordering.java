package stingion.ishop.database.map;

import stingion.ishop.database.util.OrderStatus;

import javax.persistence.*;

@Entity
@Table(name = "ordering")
public class Ordering {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(columnDefinition = "enum('pending','performed','defective')")
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "credit_card_id")
    private CreditCard creditCard;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public Ordering() {
    }

    public Ordering(Integer id, OrderStatus status, String description) {
        super();
        this.id = id;
        this.status = status;
        this.description = description;
    }

    public Ordering(OrderStatus status, String description) {
        super();
        this.status = status;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Ordering [id=" + id + ", status=" + status + ", description=" + description + ", product=" + product
                + ", creditCard=" + creditCard + "]";
    }
}
