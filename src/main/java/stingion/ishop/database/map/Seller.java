package stingion.ishop.database.map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "seller")
public class Seller {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(name = "product_seller", joinColumns = { @JoinColumn(name = "seller_id") }, inverseJoinColumns = { @JoinColumn(name = "product_id") })
    private Set<Product> productsBySeller = new HashSet<Product>();

    public Set<Product> getProductsBySeller() {
        return productsBySeller;
    }

    public void setProductsBySeller(Set<Product> productsBySeller) {
        this.productsBySeller = productsBySeller;
    }

    @ManyToMany(mappedBy = "sellersByProduct")
    private Set<Product> productsByProduct = new HashSet<Product>();

    public Set<Product> getProductsByProduct() {
        return productsByProduct;
    }

    public void setProductsByProduct(Set<Product> productsByProduct) {
        this.productsByProduct = productsByProduct;
    }

    public Seller() {
    }

    public Seller(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public Seller(String name) {
        super();
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Seller [id=" + id + ", name=" + name + "]";
    }
}
