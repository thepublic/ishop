package stingion.ishop.database.map;

import stingion.ishop.database.util.CardType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "credit_card")
public class CreditCard {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "number")
    private Long number;

    @Column(columnDefinition = "enum('MasterCard','Visa')")
    @Enumerated(EnumType.STRING)
    private CardType type;

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    @ManyToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @OneToMany(mappedBy = "creditCard")
    private Set<Ordering> orderings;

    public Set<Ordering> getOrderings() {
        return orderings;
    }

    public void setOrderings(Set<Ordering> orderings) {
        this.orderings = orderings;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CreditCard() {
    }

    public CreditCard(Integer id, Long number, CardType type) {
        super();
        this.id = id;
        this.number = number;
        this.type = type;
    }

    public CreditCard(Long number, CardType type) {
        super();
        this.number = number;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "CreditCard [id=" + id + ", number=" + number + ", type=" + type + ", bank=" + bank + ", user="
                + user + "]";
    }
}
