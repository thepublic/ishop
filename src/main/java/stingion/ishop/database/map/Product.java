package stingion.ishop.database.map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @OneToMany(mappedBy = "product")
    private Set<Ordering> orderings;

    public Set<Ordering> getOrderings() {
        return orderings;
    }

    public void setOrderings(Set<Ordering> orders) {
        this.orderings = orders;
    }

    @ManyToMany
    @JoinTable(name = "product_seller", joinColumns = {@JoinColumn(name = "product_id")}, inverseJoinColumns = {@JoinColumn(name = "seller_id")})
    private Set<Seller> sellersByProduct = new HashSet<Seller>();

    public void setSellersByProduct(Set<Seller> sellersByProduct) {
        this.sellersByProduct = sellersByProduct;
    }

    public Set<Seller> getSellersByProduct() {
        return sellersByProduct;
    }

    @ManyToMany(mappedBy = "productsBySeller")
    private Set<Seller> sellersBySeller = new HashSet<Seller>();

    public Set<Seller> getSellersBySeller() {
        return sellersBySeller;
    }

    public void setSellersBySeller(Set<Seller> sellersBySeller) {
        this.sellersBySeller = sellersBySeller;
    }

    public Product() {
    }

    public Product(Integer id, String name, Integer price) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Product(String name, Integer price) {
        super();
        this.name = name;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", name=" + name + ", price=" + price + "]";
    }
}

