package stingion.ishop.database.map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@SuppressWarnings("serial")
@Entity
@Table(name = "product_seller")
public class ProductSeller implements Serializable {

    @Id
    @Column(name = "product_id")
    private Integer product_id;

    @Id
    @Column(name = "seller_id")
    private Integer seller_id;

    public ProductSeller() {
    }

    public ProductSeller(Integer product_id, Integer seller_id) {
        super();
        this.product_id = product_id;
        this.seller_id = seller_id;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public Integer getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(Integer seller_id) {
        this.seller_id = seller_id;
    }

    @Override
    public String toString() {
        return "ProductSeller [product_id=" + product_id + ", seller_id=" + seller_id + "]";
    }
}
