package stingion.ishop.database.dao;

import org.springframework.stereotype.Repository;
import stingion.ishop.database.map.Product;

import java.util.List;

@Repository("productDao")
public class ProductDaoImpl extends HibernateDaoSupportCrud implements
		ProductDao {

	public Product getProductById(int id) {
		List list = getHibernateTemplate().find("from Product where id=?", id);
		return (Product) list.get(0);
	}

	public Product getProductByName(String name) {
		List list = getHibernateTemplate().find("from Product where name=?",
				name);
		return (Product) list.get(0);
	}

	public Product getProductByPrice(Integer price) {
		List list = getHibernateTemplate().find("from Product where price=?",
				price);
		return (Product) list.get(0);
	}

	public List<Product> getProductList() {
		return (List<Product>) getHibernateTemplate().find("from Product");
	}

}
