package stingion.ishop.database.dao;

import stingion.ishop.database.map.Seller;

public interface SellerDao extends CrudDao {

	Seller getSellerById(int id);

	Seller getSellerByName(String name);
}
