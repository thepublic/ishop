package stingion.ishop.database.dao;

public interface CrudDao {
	void save(Object obj);

	void update(Object obj);

	void delete(Object obj);
}
