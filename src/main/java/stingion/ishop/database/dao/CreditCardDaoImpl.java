package stingion.ishop.database.dao;

import org.springframework.stereotype.Repository;
import stingion.ishop.database.map.CreditCard;

import java.util.List;

@Repository("creditCardDao")
public class CreditCardDaoImpl extends HibernateDaoSupportCrud implements
        CreditCardDao {
    public CreditCard getCreditCardById(Integer id) {
        List list = getHibernateTemplate().find("from CreditCard where id=?", id);
        return (CreditCard) list.get(0);
    }

    public List<CreditCard> getCreditCardList() {
        return (List<CreditCard>) getHibernateTemplate().find("from CreditCard");
    }
}
