package stingion.ishop.database.dao;

import stingion.ishop.database.map.User;

public interface UserDao extends CrudDao {

	User getUserById(int id);

	User getUserByName(String name);
}
