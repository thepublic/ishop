package stingion.ishop.database.dao;

import stingion.ishop.database.map.Product;

import java.util.List;

public interface ProductDao extends CrudDao {

	Product getProductById(int id);

	Product getProductByName(String name);

	Product getProductByPrice(Integer name);

	List<Product> getProductList();
}
