package stingion.ishop.database.dao;

import org.springframework.stereotype.Repository;
import stingion.ishop.database.map.Seller;

import java.util.List;

@Repository("sellerDao")
public class SellerDaoImpl extends HibernateDaoSupportCrud implements SellerDao {

	public Seller getSellerById(int id) {
		List list = getHibernateTemplate().find("from Seller where id=?", id);
		return (Seller) list.get(0);
	}

	public Seller getSellerByName(String name) {
		List list = getHibernateTemplate().find("from Seller where name=?",
				name);
		return (Seller) list.get(0);
	}
}
