package stingion.ishop.database.dao;

import org.springframework.stereotype.Repository;
import stingion.ishop.database.map.User;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends HibernateDaoSupportCrud implements UserDao {

	public User getUserById(int id) {
		List list = getHibernateTemplate().find("from User where id=?", id);
		return (User) list.get(0);
	}

	public User getUserByName(String name) {
		List list = getHibernateTemplate().find("from User where name=?", name);
		return (User) list.get(0);
	}
}
