package stingion.ishop.database.dao;

import stingion.ishop.database.map.Bank;

public interface BankDao extends CrudDao {

	Bank getBankById(int id);

	Bank getBankByName(String name);
}
