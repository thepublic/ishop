package stingion.ishop.database.dao;

import org.springframework.stereotype.Repository;
import stingion.ishop.database.map.Bank;

import java.util.List;

@Repository("bankDao")
public class BankDaoImpl extends HibernateDaoSupportCrud implements BankDao {

	public Bank getBankById(int id) {
		@SuppressWarnings("rawtypes")
		List list = getHibernateTemplate().find("from Bank where id=?", id);
		return (Bank) list.get(0);
	}

	public Bank getBankByName(String name) {
		@SuppressWarnings("rawtypes")
		List list = getHibernateTemplate().find("from Bank where name=?", name);
		return (Bank) list.get(0);
	}
}
