package stingion.ishop.database.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateDaoSupportCrud extends HibernateDaoSupport implements CrudDao {
    public void save(Object obj) {
        getHibernateTemplate().save(obj);
    }

    public void update(Object obj) {
        getHibernateTemplate().update(obj);

    }

    public void delete(Object obj) {
        getHibernateTemplate().delete(obj);
    }

    @Autowired
    public void settingSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
