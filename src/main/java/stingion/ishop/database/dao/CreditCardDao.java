package stingion.ishop.database.dao;

import stingion.ishop.database.map.CreditCard;

import java.util.List;

public interface CreditCardDao extends CrudDao {
	CreditCard getCreditCardById(Integer id);

	List<CreditCard> getCreditCardList();
}
