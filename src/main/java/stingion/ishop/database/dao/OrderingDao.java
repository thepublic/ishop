package stingion.ishop.database.dao;

import stingion.ishop.database.map.Ordering;

public interface OrderingDao extends CrudDao {

	Ordering getOrderingById(Integer id);
	
}