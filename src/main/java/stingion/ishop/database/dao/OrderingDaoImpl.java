package stingion.ishop.database.dao;

import org.springframework.stereotype.Repository;
import stingion.ishop.database.map.Ordering;

import java.util.List;

@Repository("orderingDao")
public class OrderingDaoImpl extends HibernateDaoSupportCrud implements OrderingDao {

	public Ordering getOrderingById(Integer id) {
		List list = getHibernateTemplate().find("from Ordering where id=?", id);
		return (Ordering) list.get(0);
	}
}