package stingion.ishop.database.util;

public enum OrderStatus {
    pending, performed, defective
}
