package stingion.ishop.database;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: ievgen
 * Date: 3/14/14
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class CreationDB {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context/database-creation.xml");

        context.getBean("scriptExecStruct");
        context.getBean("scriptExecData");
    }
}
