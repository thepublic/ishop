package stingion.ishop.database;

import stingion.utils.SQLScriptExecutor;

public class ExecScript {

	public static Boolean executeScript(SQLScriptExecutor sse, String scriptPath) {
		return sse.execute(scriptPath);
	}
}
