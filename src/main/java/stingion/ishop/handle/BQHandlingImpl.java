package stingion.ishop.handle;

import stingion.ishop.database.map.Ordering;
import stingion.ishop.database.util.OrderStatus;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class BQHandlingImpl implements BQHandling<Ordering> {
	private BlockingQueue<Ordering> queue;
	private int waitingInterval;

	public int getWaitingInterval() {
		return waitingInterval;
	}

	public void setWaitingInterval(int waitingInterval) {
		this.waitingInterval = waitingInterval;
	}

	public BlockingQueue<Ordering> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<Ordering> queue) {
		this.queue = queue;
	}

	public Ordering handle() throws InterruptedException {
		Ordering ordering = queue.poll(waitingInterval, TimeUnit.MILLISECONDS);
		if (ordering != null && ordering.getStatus() == OrderStatus.pending) {
			ordering.setStatus(OrderStatus.performed);
		}
		return ordering;
	}
}
