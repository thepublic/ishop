package stingion.ishop.handle;

public interface Handling<T> {
	T handle() throws InterruptedException;
}
