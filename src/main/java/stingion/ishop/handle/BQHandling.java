package stingion.ishop.handle;

import java.util.concurrent.BlockingQueue;

public interface BQHandling<T> extends Handling<T> {
	BlockingQueue<T> getQueue();

	void setQueue(BlockingQueue<T> bq);
}
