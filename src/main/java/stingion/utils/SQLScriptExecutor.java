package stingion.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLScriptExecutor {

	private String url;

	private static Connection con;
	private static Statement stmt;

	public SQLScriptExecutor(String url) {
		this.url = url;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException(ex);
		}
	}

	public boolean execute(String scriptPath) {

		boolean result = true;

		try {
			con = DriverManager.getConnection(url);
			stmt = con.createStatement();

			String fullScript = readFileToStringVar(scriptPath);
			String[] scriptStmts = fullScript.split(";");

			for (int i = 0; i < scriptStmts.length; i++) {
				result &= stmt.execute(scriptStmts[i]);
			}
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
            throw new RuntimeException(ex);
		}
		return result;
	}

	private static String readFileToStringVar(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}
}
