drop database if exists `ishop`;

create database `ishop`;

use `ishop`;

create table `bank` (
    `id` int not null auto_increment,
    `name` varchar(30),
    primary key (`id`)
);

create table `user` (
    `id` int not null auto_increment,
    `name` varchar(30),
    primary key (`id`)
);

create table `credit_card` (
    `id` int not null auto_increment,
    `number` bigint,
    `type` enum('MasterCard', 'Visa'),
    `bank_id` int,
    `user_id` int,
    primary key (`id`),
    foreign key (`bank_id`)
        references `bank` (`id`),
    foreign key (`user_id`)
        references `user` (`id`)
);

create table `product` (
    `id` int not null auto_increment,
    `name` varchar(50),
    `price` int,
    primary key (`id`)
);

create table `seller` (
    `id` int not null auto_increment,
    `name` varchar(50),
    primary key (`id`)
);

create table `product_seller` (
    `product_id` int,
    `seller_id` int,
    primary key (`product_id` , `seller_id`),
    foreign key (`product_id`)
        references `product` (`id`),
    foreign key (`seller_id`)
        references `seller` (`id`)
);

create table `ordering` (
    `id` int not null auto_increment,
    `product_id` int,
    `status` enum('pending', 'performed', 'defective'),
    `description` varchar(50),
    `credit_card_id` int,
    primary key (`id`),
    foreign key (`credit_card_id`)
        references `credit_card` (`id`),
    foreign key (`product_id`)
        references `product` (`id`)
);