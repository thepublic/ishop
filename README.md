#IShop

*The project of product orders imitation in multi-threading mode.
Java multithreading; Spring; Hibernate; MySQL RDBMS technologies are used here.*

##Software for installing
+ Mysql server and Workbench
+ Apache ActiveMQ JMS
+ IDE (e.g. Eclipse, Intellij Idea)

##Configuring and run project
1) First of all run MySQL server (if you don't have user and password for database configure it)

2) Open properties files:
*/src/main/resources/application.properties* and
*/src/test/resources/application.properties*. Change properties: *db.domain_port*, *db.username*, *db.password*
on your own database username and password

3) Following instruction depends on branch that you prefer to work with:
#####*If you work with "master" branch*

- To create database *ishop* and fill it with input content run class:
```java
stingion.ishop.database.CreationDB
```
But it isn't obligatory for you to do this step, because database will be created automatically
while next step are executed

- To execute application run class:
```java
stingion.ishop.Starter
```
For to see how ordering handling in process of execution see table *ishop.ordering* in database with frequent refreshing

- To drop database *ishop* run class:
```java
stingion.ishop.database.DropingDB
```

#####*If you working with "transaction_model" branch*
